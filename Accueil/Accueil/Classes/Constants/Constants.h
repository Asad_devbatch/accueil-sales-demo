//
//  Constants.h
//  Accueil
//
//  Created by MacPro on 04/02/2015.
//  Copyright (c) 2015 MacPro. All rights reserved.
//

#ifndef Accueil_Constants_h
#define Accueil_Constants_h

#define IS_IOS6_AND_UP ([[UIDevice currentDevice].systemVersion floatValue] >= 6.0)

#define IS_IOS7_AND_UP ([[UIDevice currentDevice].systemVersion floatValue] >= 7.0)

#define IS_IOS8_AND_UP ([[UIDevice currentDevice].systemVersion floatValue] >= 8.0)

#define NavBarMenuImage [UIImage imageNamed:@"Nav_Menu"]
#define NavBarFilterImage [UIImage imageNamed:@"Nav_Filter"]

#define kACMenuViewWidth 300
#define kACMenuViewHeight 31
#define kACMenuViewWidthButton 100
#define kACMenuViewHorizontalMargin 0
#define kACMenuViewVerticalMargin 8
#define kACMenuViewInternalPadding 0
#define kACRatingViewWidth 120
#define kACRatingViewHeight 24
#endif

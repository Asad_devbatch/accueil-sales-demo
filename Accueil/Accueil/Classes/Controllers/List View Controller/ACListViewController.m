//
//  ACListViewController.m
//  Accueil
//
//  Created by MacPro on 04/02/2015.
//  Copyright (c) 2015 MacPro. All rights reserved.
//

#import "ACListViewController.h"

@interface ACListViewController ()

@end

@implementation ACListViewController

#pragma mark -
#pragma mark UIViewController Lifecycle
#pragma mark -

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //Initializations
    [self initializations];
    
    //SetupView
    [self setUpView];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}
- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    [self.menuView layoutIfNeeded];
}
- (void)setUpView
{
    [self setupNavigationBarTitle:@"" showTitleView:NO showRightButton:YES leftButtonType:UINavigationBarLeftButtonTypeMenu rightButtonType:UINavigationBarRightButtonTypeFilter];
    
    [self setUpLocationButton];
    
    [self setUpMenu];

    list = @[ @"Carpaccio de boeuf", @"Côtes de porc et pommes de terre", @"Farfalles au pesto", @"Carpaccio de boeuf", @"Côtes de porc et pommes de terre", @"Farfalles au pesto", @"Carpaccio de boeuf", @"Côtes de porc et pommes de terre", @"Farfalles au pesto", @"Carpaccio de boeuf", @"Côtes de porc et pommes de terre", @"Farfalles au pesto"];

}
- (void)setUpMenu
{
    self.menuView.delegate = self;
    self.menuView.dataSource = self;
    
    self.menuItems = @[@"Goûts",
                       @"Temps",
                       @"Carte"];
    
    [self.menuView initMenu];
    
}

- (void)initializations
{
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}



#pragma mark - ACMenuViewDataSource Protocol Methods

- (NSInteger)numberOfItemsInMenuList:(ACMenuView *)menuList {
    return self.menuItems.count;
}

- (NSString *)menuList:(ACMenuView *)menuList titleForItemWithIndex:(NSInteger)index {
    return self.menuItems[index];
}

#pragma mark -ACMenuViewDelegate Protocol Methods

- (void)menuList:(ACMenuView *)menuList didSelectButtonWithIndex:(NSInteger)index {
    NSLog(@"%@ selected",self.menuItems[index]);
}


#pragma mark -
#pragma mark - UITableView Datasource
#pragma mark -


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return list.count;
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 160.0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView1 cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"MenuList";
    UITableViewCell *cell = [tableView1 dequeueReusableCellWithIdentifier:cellIdentifier];
    
    UIImageView *imageView = (UIImageView *)[cell.contentView viewWithTag:1];
    [imageView setContentMode:UIViewContentModeScaleToFill];
    [imageView setImage:[UIImage imageNamed:[NSString stringWithFormat:@"img_%ld",(long)indexPath.row%3]]];
    UILabel *lblTitle = (UILabel *)[cell.contentView viewWithTag:2];
    [lblTitle setText:[list objectAtIndex:indexPath.row]];
    UILabel *lblDistance = (UILabel *)[cell.contentView viewWithTag:3];
    [lblDistance setText:@"1.6 km"];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
//    ACRatingView *ratingView = (ACRatingView *)[cell.contentView viewWithTag:4];
//    [ratingView setImageFullWithName:@"rate_filled"];
//    [ratingView setImageEmptyWithName:@"rate_empty"];
    
//    ACRatingView *priceView = (ACRatingView *)[cell.contentView viewWithTag:5];
//    [priceView setImageFullWithName:@"currency_filled"];
//    [priceView setImageEmptyWithName:@"currency_empty"];
    
    return cell;
}

#pragma mark - UITableView Delegate methods

- (void)tableView:(UITableView *)tableView1 didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}


#pragma mark - ACBaseViewController Delegate

- (void)rightNavigationBarButtonClicked
{
}

- (void)leftNavigationBarButtonClicked
{
}

- (void)locationButtonClicked {
    
}

@end

//
//  ACListViewController.h
//  Accueil
//
//  Created by MacPro on 04/02/2015.
//  Copyright (c) 2015 MacPro. All rights reserved.
//

#import "ACBaseViewController.h"
#import "ACMenuView.h"
#import "ACRatingView.h"
@interface ACListViewController : ACBaseViewController <ACMenuViewDataSource,ACMenuViewDelegate,ACBaseViewControllerDelegate>
{
    NSArray *list;
}
@property (nonatomic, strong) IBOutlet ACMenuView *menuView;
@property (nonatomic, strong) NSArray *menuItems;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

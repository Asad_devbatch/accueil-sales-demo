//
//  ACBaseViewController.m
//  Accueil
//
//  Created by MacPro on 04/02/2015.
//  Copyright (c) 2015 MacPro. All rights reserved.
//

#import "ACBaseViewController.h"

@interface ACBaseViewController ()

@end

@implementation ACBaseViewController

- (void)viewDidLoad {
  [super viewDidLoad];
  // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
  [super didReceiveMemoryWarning];
  // Dispose of any resources that can be recreated.
}

- (void)setupNavigationBarTitle:(NSString*)title
                  showTitleView:(BOOL)showTitleView
                showRightButton:(BOOL)showRightButton
                 leftButtonType:(UINavigationBarLeftButtonType)leftButtonType
                rightButtonType:
                    (UINavigationBarRightButtonType)rightButtonType {
  if (showRightButton) {
    UIImage* optionsImage =
        (rightButtonType == UINavigationBarRightButtonTypeFilter)
            ? NavBarFilterImage
            : nil;

    if (!rightButton)
      rightButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 44, 44)];
    [rightButton addTarget:self
                    action:@selector(rightButtonClicked:)
          forControlEvents:UIControlEventTouchUpInside];
    [rightButton setImage:optionsImage forState:UIControlStateNormal];
    [rightButton setImage:nil forState:UIControlStateSelected];

    rightButton.contentMode = UIViewContentModeScaleAspectFit;
    rightbtnItem = [[ACBarButtonItem alloc] initWithCustomButton:rightButton];
    UIBarButtonItem* negativeSpacer = [[UIBarButtonItem alloc]
        initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                             target:nil
                             action:nil];
    negativeSpacer.width = IS_IOS6_AND_UP ? -6 : -16;
    [self.navigationItem
        setRightBarButtonItems:[NSArray arrayWithObjects:negativeSpacer,
                                                         rightbtnItem, nil]
                      animated:NO];
  } else {
    rightButton.hidden = YES;
  }

  if (leftButtonType != UINavigationBarLeftButtonTypeNone) {
    if (!leftButton) leftButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 44, 44)];
    UIImage* menuImage = (leftButtonType == UINavigationBarLeftButtonTypeMenu)
                             ? NavBarMenuImage
                             : nil;
    [leftButton addTarget:self
                   action:@selector(leftButtonClicked:)
         forControlEvents:UIControlEventTouchUpInside];

    [leftButton setImage:menuImage forState:UIControlStateNormal];
    [leftButton setImage:nil forState:UIControlStateSelected];

    leftButton.contentMode = UIViewContentModeScaleAspectFit;
    leftbtnItem = [[ACBarButtonItem alloc] initWithCustomButton:leftButton];

    UIBarButtonItem* negativeSpacer = [[UIBarButtonItem alloc]
        initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                             target:nil
                             action:nil];

    negativeSpacer.width = IS_IOS6_AND_UP ? -6 : -16;
    [self.navigationItem
        setLeftBarButtonItems:[NSArray arrayWithObjects:negativeSpacer,
                                                        leftbtnItem, nil]
                     animated:NO];
  } else {
    leftButton.hidden = YES;
  }

  [self setUpSearchBar];

    if (showTitleView) {
      self.navigationItem.titleView =
          [[UIImageView alloc] initWithImage:[UIImage imageNamed:@""]];
    } else if (title != nil) {
      self.navigationItem.title = title;
    }
}



- (void)setUpSearchBar {
  if (!searchBar)
    searchBar = [[UISearchBar alloc]
        initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 44)];
  [searchBar setDelegate:self];
  [searchBar setReturnKeyType:UIReturnKeyDone];
  searchBar.enablesReturnKeyAutomatically = NO;
  self.navigationItem.titleView = searchBar;
}

#pragma mark -
#pragma mark Buttons Action
#pragma mark -

- (void)rightButtonClicked:(id)sender {
  if (self.baseDelegate &&
      [self.baseDelegate
          respondsToSelector:@selector(rightNavigationBarButtonClicked)]) {
    [self.baseDelegate rightNavigationBarButtonClicked];
  }
}
- (void)leftButtonClicked:(id)sender {
  if (self.baseDelegate &&
      [self.baseDelegate
          respondsToSelector:@selector(leftNavigationBarButtonClicked)]) {
    [self.baseDelegate leftNavigationBarButtonClicked];
  }
}
- (void)locationButtonClicked:(id)sender {
  if (self.baseDelegate &&
      [self.baseDelegate respondsToSelector:@selector(locationButtonClicked)]) {
    [self.baseDelegate locationButtonClicked];
  }
}

- (void)setUpLocationButton {
  locationButton = [[UIButton alloc] init];
  [locationButton addTarget:self
                     action:@selector(leftButtonClicked:)
           forControlEvents:UIControlEventTouchUpInside];
  [locationButton setImage:[UIImage imageNamed:@"location_icon"]
                  forState:UIControlStateNormal];
  [locationButton setTranslatesAutoresizingMaskIntoConstraints:NO];
  [self.view addSubview:locationButton];

  [self.view addConstraint:[NSLayoutConstraint
                               constraintWithItem:locationButton
                                        attribute:NSLayoutAttributeCenterX
                                        relatedBy:NSLayoutRelationEqual
                                           toItem:self.view
                                        attribute:NSLayoutAttributeCenterX
                                       multiplier:1.0
                                         constant:0.0]];

  [self.view addConstraint:[NSLayoutConstraint
                               constraintWithItem:locationButton
                                        attribute:NSLayoutAttributeBottom
                                        relatedBy:NSLayoutRelationEqual
                                           toItem:self.view
                                        attribute:NSLayoutAttributeBottom
                                       multiplier:1.0
                                         constant:-20.0]];
}

- (void)searchBarSearchButtonClicked:(UISearchBar*)searchBar1 {
    [searchBar1 resignFirstResponder];
}

@end

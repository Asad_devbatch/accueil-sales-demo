//
//  ACBaseViewController.h
//  Accueil
//
//  Created by MacPro on 04/02/2015.
//  Copyright (c) 2015 MacPro. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ACBarButtonItem.h"
typedef enum {
    UINavigationBarRightButtonTypeNone,
    UINavigationBarRightButtonTypeFilter,
} UINavigationBarRightButtonType;

typedef enum {
    UINavigationBarLeftButtonTypeNone,
    UINavigationBarLeftButtonTypeMenu,
} UINavigationBarLeftButtonType;

@protocol ACBaseViewControllerDelegate <NSObject>

@optional

- (void)rightNavigationBarButtonClicked;
- (void)leftNavigationBarButtonClicked;
- (void)locationButtonClicked;

@end


@interface ACBaseViewController : UIViewController

<UISearchBarDelegate>
{
    UIButton *leftButton;
    UIButton *locationButton;
    UIButton *rightButton;
    ACBarButtonItem* rightbtnItem;
    ACBarButtonItem* leftbtnItem;
    UISearchBar *searchBar;
}
@property (nonatomic, assign) id<ACBaseViewControllerDelegate> baseDelegate;

- (void)setupNavigationBarTitle:(NSString*)title showTitleView:(BOOL)showTitleView showRightButton:(BOOL)showRightButton leftButtonType:(UINavigationBarLeftButtonType)leftButtonType rightButtonType:(UINavigationBarRightButtonType)rightButtonType;

- (void) setUpLocationButton;


@end

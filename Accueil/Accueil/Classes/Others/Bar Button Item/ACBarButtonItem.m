//
//  ACBarButtonItem.m
//  Accueil
//
//  Created by MacPro on 11/02/2015.
//  Copyright (c) 2015 MacPro. All rights reserved.
//

#import "ACBarButtonItem.h"

@implementation ACBarButtonItem
- (ACBarButtonItem *)initWithCustomButton:(UIButton *)customButton
{
    self = [self initWithCustomView:customButton];
    if (self) {
        [self initializer];
    }
    
    return self;
}
- (void)initializer
{
    self.customView.clipsToBounds = NO;
    if (!self.badgeView) {
        self.badgeView                      = [[UIView alloc] initWithFrame:CGRectMake(27, 8, 10, 10)];
        [self.badgeView setBackgroundColor:[UIColor colorWithRed:0.92 green:0.36 blue:0.43 alpha:1]];
        [self.badgeView.layer setCornerRadius:5];
        [self.customView addSubview:self.badgeView];
    }
}

- (void)hideBadgeView : (BOOL) hide {
    [self.badgeView  setHidden:hide];
}

@end

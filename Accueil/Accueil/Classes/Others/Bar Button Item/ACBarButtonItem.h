//
//  ACBarButtonItem.h
//  Accueil
//
//  Created by MacPro on 11/02/2015.
//  Copyright (c) 2015 MacPro. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ACBarButtonItem : UIBarButtonItem

@property (nonatomic,retain) UIView *badgeView;

- (ACBarButtonItem *)initWithCustomButton:(UIButton *)customButton;
- (void)hideBadgeView : (BOOL) hide;
@end

//
//  ACRatingView.h
//  Accueil
//
//  Created by MacPro on 04/02/2015.
//  Copyright (c) 2015 MacPro. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ACRatingView : UIView
{
    UIImage* fullImage;
    UIImage* emptyImage;
    NSMutableArray* ratingViewArray;
}

@property (nonatomic) NSInteger rating;

-(void)setImageFullWithName:(NSString*)name;
-(void)setImageEmptyWithName:(NSString*)name;

@end

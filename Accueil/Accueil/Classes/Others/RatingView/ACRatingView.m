
//  ACRatingView.m
//  Accueil
//
//  Created by MacPro on 04/02/2015.
//  Copyright (c) 2015 MacPro. All rights reserved.
//

#import "ACRatingView.h"

@implementation ACRatingView

@synthesize rating = _rating;

-(void)setRating:(NSInteger)rating
{
    _rating = rating;
    
    for (NSInteger i = 0; i < _rating; i++)
    {
        UIButton* btn = [ratingViewArray objectAtIndex:i];
        btn.selected=YES;
    }
    
    for (NSInteger i = _rating; i < ratingViewArray.count; i++)
    {
        UIButton* btn = [ratingViewArray objectAtIndex:i];
        btn.selected=NO;
    }
}

-(NSInteger)rating
{
    return _rating;
}

-(void)setImageFullWithName:(NSString*)name
{
    fullImage = [UIImage imageNamed:name];
    
}
-(void)setImageEmptyWithName:(NSString*)name
{
    emptyImage = [UIImage imageNamed:name];
}
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        fullImage = [UIImage imageNamed:@"ratingFullIcon"];
        emptyImage = [UIImage imageNamed:@"ratingEmptyIcon"];
        
        ratingViewArray = [NSMutableArray array];
        
        for (int i = 0; i < 5; i++)
        {
            UIButton* btn = [UIButton buttonWithType:UIButtonTypeCustom];
            [btn setImage:emptyImage forState:UIControlStateNormal];
            [btn setImage:fullImage forState:UIControlStateSelected];
            btn.tag = i+1;
            [btn addTarget:self action:@selector(btn_pressed:) forControlEvents:UIControlEventTouchUpInside];
            [self addSubview:btn];
            
            
            [ratingViewArray addObject:btn];
        }
        
    }
    UITapGestureRecognizer *oneFingerTwoTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGesture:)];
    [oneFingerTwoTap setNumberOfTapsRequired:1];
    [oneFingerTwoTap setNumberOfTouchesRequired:1];
    [self addGestureRecognizer:oneFingerTwoTap];
    
    return self;
}
- (void)tapGesture:(id)sender
{
    [self setRating:0];
}
- (void)btn_pressed:(id)sender
{
    UIButton *img = sender;
    NSLog(@"Integer : %ld", (long)img.tag);
    [self setRating:img.tag];
}
-(void)layoutSubviews
{
    [super layoutSubviews];
    int size = kACRatingViewHeight;
    int x = kACRatingViewWidth / 2 - size * (float)ratingViewArray.count/2;
    for (UIButton* btn in ratingViewArray)
    {
        btn.frame = CGRectMake(x, 0, size, size);
        x+=size;
    }
}

- (id)initWithCoder:(NSCoder*)aDecoder
{
    if(self = [super initWithCoder:aDecoder]) {
        // Do something
//        fullImage = [UIImage imageNamed:@"ratingFullIcon"];
//        emptyImage = [UIImage imageNamed:@"ratingEmptyIcon"];
        
        if (self.tag == 4) {
            emptyImage = [UIImage imageNamed:@"rate_empty"];
            fullImage = [UIImage imageNamed:@"rate_filled"];
        }else if (self.tag == 5) {
            emptyImage = [UIImage imageNamed:@"currency_empty"];
            fullImage = [UIImage imageNamed:@"currency_filled"];
        }
        
        ratingViewArray = [NSMutableArray array];
        
        for (int i = 0; i < 5; i++)
        {
            UIButton* btn = [UIButton buttonWithType:UIButtonTypeCustom];
            [btn setImage:emptyImage forState:UIControlStateNormal];
            [btn setImage:fullImage forState:UIControlStateSelected];
            btn.tag = i+1;
            [btn addTarget:self action:@selector(btn_pressed:) forControlEvents:UIControlEventTouchUpInside];
            [self addSubview:btn];
            
            
            [ratingViewArray addObject:btn];
        }
    }
    
    [self.superview  addConstraint:[NSLayoutConstraint
                    constraintWithItem:self
                    attribute:NSLayoutAttributeHeight
                    relatedBy:NSLayoutRelationEqual
                    toItem:nil
                    attribute:NSLayoutAttributeNotAnAttribute
                    multiplier:1.0
                    constant:kACRatingViewHeight]];
    
    [self.superview addConstraint:[NSLayoutConstraint
                    constraintWithItem:self
                    attribute:NSLayoutAttributeWidth
                    relatedBy:NSLayoutRelationEqual
                    toItem:nil
                    attribute:NSLayoutAttributeNotAnAttribute
                    multiplier:1.0
                    constant:kACRatingViewWidth]];
    
    
    UITapGestureRecognizer *oneFingerTwoTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGesture:)];
    [oneFingerTwoTap setNumberOfTapsRequired:1];
    [oneFingerTwoTap setNumberOfTouchesRequired:1];
    [self addGestureRecognizer:oneFingerTwoTap];
    
    return self;
}
@end

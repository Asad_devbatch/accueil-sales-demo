//
//  ACMenuView.m
//  Accueil
//
//  Created by MacPro on 04/02/2015.
//  Copyright (c) 2015 MacPro. All rights reserved.
//

#import "ACMenuView.h"
#import "ACMenuScrollView.h"

@interface ACMenuView ()

@property(nonatomic, strong) UIScrollView* scrollView;
@property(nonatomic, strong) NSMutableArray* buttons;
@property(nonatomic, strong) UIView* contentView;

@end

@implementation ACMenuView

- (id)initWithFrame:(CGRect)frame {
  self = [super initWithFrame:frame];
  if (self) {
    self.backgroundColor =
        [UIColor colorWithRed:0.68 green:0.84 blue:0.79 alpha:1];

    UIImageView* imageUnSelected =
        [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"nav_bg"]];
    imageUnSelected.translatesAutoresizingMaskIntoConstraints = NO;
    [self addSubview:imageUnSelected];

    [self addConstraint:[NSLayoutConstraint
                            constraintWithItem:imageUnSelected
                                     attribute:NSLayoutAttributeLeading
                                     relatedBy:NSLayoutRelationEqual
                                        toItem:self
                                     attribute:NSLayoutAttributeLeading
                                    multiplier:1.0
                                      constant:kACMenuViewHorizontalMargin]];

    [self addConstraint:[NSLayoutConstraint
                            constraintWithItem:imageUnSelected
                                     attribute:NSLayoutAttributeTrailing
                                     relatedBy:NSLayoutRelationEqual
                                        toItem:self
                                     attribute:NSLayoutAttributeTrailing
                                    multiplier:1.0
                                      constant:-kACMenuViewHorizontalMargin]];

    [self addConstraint:[NSLayoutConstraint
                            constraintWithItem:imageUnSelected
                                     attribute:NSLayoutAttributeCenterY
                                     relatedBy:NSLayoutRelationEqual
                                        toItem:self
                                     attribute:NSLayoutAttributeCenterY
                                    multiplier:1.0
                                      constant:0.0]];

    [self addConstraint:[NSLayoutConstraint
                            constraintWithItem:imageUnSelected
                                     attribute:NSLayoutAttributeHeight
                                     relatedBy:NSLayoutRelationEqual
                                        toItem:nil
                                     attribute:NSLayoutAttributeNotAnAttribute
                                    multiplier:1.0
                                      constant:31.]];

    _scrollView = [[ACMenuScrollView alloc] init];
    _scrollView.backgroundColor = [UIColor clearColor];
    _scrollView.showsHorizontalScrollIndicator = NO;
    [_scrollView setScrollEnabled:NO];
    _scrollView.scrollsToTop = NO;
    _scrollView.canCancelContentTouches = YES;
    _scrollView.translatesAutoresizingMaskIntoConstraints = NO;
    [self addSubview:_scrollView];

    [self
        addConstraints:
            [NSLayoutConstraint
                constraintsWithVisualFormat:@"H:|[_scrollView]|"
                                    options:
                                        NSLayoutFormatDirectionLeadingToTrailing
                                    metrics:nil
                                      views:NSDictionaryOfVariableBindings(
                                                _scrollView)]];

    [self
        addConstraints:
            [NSLayoutConstraint
                constraintsWithVisualFormat:@"V:|[_scrollView]|"
                                    options:
                                        NSLayoutFormatDirectionLeadingToTrailing
                                    metrics:nil
                                      views:NSDictionaryOfVariableBindings(
                                                _scrollView)]];

    _contentView = [[UIView alloc] init];
    _contentView.translatesAutoresizingMaskIntoConstraints = NO;
    [_scrollView addSubview:_contentView];

    [self addConstraint:[NSLayoutConstraint
                            constraintWithItem:_contentView
                                     attribute:NSLayoutAttributeTop
                                     relatedBy:NSLayoutRelationEqual
                                        toItem:self
                                     attribute:NSLayoutAttributeTop
                                    multiplier:1.0
                                      constant:0.0]];

    [self addConstraint:[NSLayoutConstraint
                            constraintWithItem:_contentView
                                     attribute:NSLayoutAttributeBottom
                                     relatedBy:NSLayoutRelationEqual
                                        toItem:self
                                     attribute:NSLayoutAttributeBottom
                                    multiplier:1.0
                                      constant:0.0]];

    [self
        addConstraints:
            [NSLayoutConstraint
                constraintsWithVisualFormat:@"H:|[_contentView]|"
                                    options:
                                        NSLayoutFormatDirectionLeadingToTrailing
                                    metrics:nil
                                      views:NSDictionaryOfVariableBindings(
                                                _contentView)]];

    [self
        addConstraints:
            [NSLayoutConstraint
                constraintsWithVisualFormat:@"V:|[_contentView]|"
                                    options:
                                        NSLayoutFormatDirectionLeadingToTrailing
                                    metrics:nil
                                      views:NSDictionaryOfVariableBindings(
                                                _contentView)]];
    _buttons = [NSMutableArray array];
  }
  return self;
}

- (void)initMenu {
    self.backgroundColor =
    [UIColor colorWithRed:0.68 green:0.84 blue:0.79 alpha:1];
    
    _scrollView = [[ACMenuScrollView alloc] init];
    _scrollView.backgroundColor = [UIColor clearColor];
    _scrollView.showsHorizontalScrollIndicator = NO;
    [_scrollView setScrollEnabled:NO];
    _scrollView.scrollsToTop = NO;
    _scrollView.canCancelContentTouches = YES;
    _scrollView.translatesAutoresizingMaskIntoConstraints = NO;
    [self addSubview:_scrollView];
    
    [self
     addConstraints:
     [NSLayoutConstraint
      constraintsWithVisualFormat:@"H:|[_scrollView]|"
      options:
      NSLayoutFormatDirectionLeadingToTrailing
      metrics:nil
      views:NSDictionaryOfVariableBindings(_scrollView)]];
    
    [self
     addConstraints:
     [NSLayoutConstraint
      constraintsWithVisualFormat:@"V:|[_scrollView]|"
      options:
      NSLayoutFormatDirectionLeadingToTrailing
      metrics:nil
      views:NSDictionaryOfVariableBindings(
                                           _scrollView)]];
    
    _contentView = [[UIView alloc] init];
    _contentView.translatesAutoresizingMaskIntoConstraints = NO;
    [_contentView setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"nav_bg"]]];
    [_scrollView addSubview:_contentView];
    
    
    [self addConstraint:[NSLayoutConstraint
                         constraintWithItem:_contentView
                         attribute:NSLayoutAttributeWidth
                         relatedBy:NSLayoutRelationEqual
                         toItem:nil
                         attribute:NSLayoutAttributeNotAnAttribute
                         multiplier:1.0
                         constant:kACMenuViewWidth]];
    
    [self addConstraint:[NSLayoutConstraint
                         constraintWithItem:_contentView
                         attribute:NSLayoutAttributeCenterX
                         relatedBy:NSLayoutRelationEqual
                         toItem:self
                         attribute:NSLayoutAttributeCenterX
                         multiplier:1.0
                         constant:0.0]];
    
    [self addConstraint:[NSLayoutConstraint
                         constraintWithItem:_contentView
                         attribute:NSLayoutAttributeHeight
                         relatedBy:NSLayoutRelationEqual
                         toItem:nil
                         attribute:NSLayoutAttributeNotAnAttribute
                         multiplier:1.0
                         constant:kACMenuViewHeight]];
    
    [self addConstraint:[NSLayoutConstraint
                         constraintWithItem:_contentView
                         attribute:NSLayoutAttributeCenterY
                         relatedBy:NSLayoutRelationEqual
                         toItem:self
                         attribute:NSLayoutAttributeCenterY
                         multiplier:1.0
                         constant:0.0]];
    
    _buttons = [NSMutableArray array];
}

- (void)reloadData {
  for (UIButton* button in self.buttons) {
    [button removeFromSuperview];
  }

  [self.buttons removeAllObjects];

  NSInteger totalButtons = [self.dataSource numberOfItemsInMenuList:self];

  if (totalButtons < 1) {
    return;
  }

  UIButton* previousButton;

  for (NSInteger index = 0; index < totalButtons; index++) {
    NSString* buttonTitle =
        [self.dataSource menuList:self titleForItemWithIndex:index];

    UIButton* button = [self selectionListButtonWithTitle:buttonTitle];
    [self.contentView addSubview:button];
    if (previousButton) {
      [self.contentView
          addConstraints:
              [NSLayoutConstraint
                  constraintsWithVisualFormat:
                      @"H:[previousButton]-padding-[button]"
                                      options:
                                          NSLayoutFormatDirectionLeadingToTrailing
                                      metrics:@{
                                        @"padding" :
                                            @(kACMenuViewInternalPadding)
                                      }
                                        views:NSDictionaryOfVariableBindings(
                                                  previousButton, button)]];
    } else {
      [self.contentView
          addConstraints:
              [NSLayoutConstraint
                  constraintsWithVisualFormat:@"H:|-margin-[button]"
                                      options:
                                          NSLayoutFormatDirectionLeadingToTrailing
                                      metrics:@{
                                        @"margin" :
                                            @(kACMenuViewHorizontalMargin)
                                      }
                                        views:NSDictionaryOfVariableBindings(
                                                  button)]];
    }

    [self.contentView
        addConstraint:[NSLayoutConstraint
                          constraintWithItem:button
                                   attribute:NSLayoutAttributeCenterY
                                   relatedBy:NSLayoutRelationEqual
                                      toItem:self.contentView
                                   attribute:NSLayoutAttributeCenterY
                                  multiplier:1.0
                                    constant:0.0]];

    [self.contentView
        addConstraint:[NSLayoutConstraint
                          constraintWithItem:button
                                   attribute:NSLayoutAttributeHeight
                                   relatedBy:NSLayoutRelationEqual
                                      toItem:nil
                                   attribute:NSLayoutAttributeNotAnAttribute
                                  multiplier:1.0
                                    constant:kACMenuViewHeight]];

    [self.contentView addConstraint:[NSLayoutConstraint
                           constraintWithItem:button
                           attribute:NSLayoutAttributeWidth
                           relatedBy:NSLayoutRelationEqual
                           toItem:nil
                           attribute:NSLayoutAttributeNotAnAttribute
                           multiplier:1.0
                           constant:kACMenuViewWidth / [self.dataSource numberOfItemsInMenuList:self]]];
      
    previousButton = button;

    [self.buttons addObject:button];
  }

  [self.contentView
      addConstraints:
          [NSLayoutConstraint
              constraintsWithVisualFormat:@"H:[previousButton]-margin-|"
                                  options:
                                      NSLayoutFormatDirectionLeadingToTrailing
                                  metrics:@{
                                    @"margin" : @(kACMenuViewHorizontalMargin)
                                  }
                                    views:NSDictionaryOfVariableBindings(
                                              previousButton)]];

  if (totalButtons > 0) {
    UIButton* selectedButton = self.buttons[self.selectedButtonIndex];
    selectedButton.selected = YES;
  }
  [self updateConstraintsIfNeeded];
}
- (void)layoutSubviews {
  if (!self.buttons.count) {
    [self reloadData];
  }

  [super layoutSubviews];
}

- (UIButton*)selectionListButtonWithTitle:(NSString*)buttonTitle {
  UIButton* button = [UIButton buttonWithType:UIButtonTypeCustom];
  [button setTitle:buttonTitle forState:UIControlStateNormal];
  [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
  [button setTitleColor:[UIColor whiteColor]
               forState:UIControlStateHighlighted];
  [button setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
  [button setBackgroundImage:[UIImage imageNamed:@"hover"]
                    forState:UIControlStateSelected];
  button.titleLabel.font = [UIFont systemFontOfSize:13];
  [button addTarget:self
                action:@selector(buttonWasTapped:)
      forControlEvents:UIControlEventTouchUpInside];

  button.translatesAutoresizingMaskIntoConstraints = NO;
  return button;
}

- (void)buttonWasTapped:(id)sender {
  NSInteger index = [self.buttons indexOfObject:sender];
  if (index != NSNotFound) {
    if (index == self.selectedButtonIndex) {
      return;
    }

    UIButton* oldSelectedButton = self.buttons[self.selectedButtonIndex];
    oldSelectedButton.selected = NO;
    self.selectedButtonIndex = index;

    UIButton* tappedButton = (UIButton*)sender;
    tappedButton.selected = YES;

    [self layoutIfNeeded];
    if ([self.delegate
            respondsToSelector:@selector(menuList:didSelectButtonWithIndex:)]) {
      [self.delegate menuList:self didSelectButtonWithIndex:index];
    }
  }
}

@end

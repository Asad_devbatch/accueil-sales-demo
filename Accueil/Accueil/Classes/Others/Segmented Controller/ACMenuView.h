//
//  ACMenuView.h
//  Accueil
//
//  Created by MacPro on 04/02/2015.
//  Copyright (c) 2015 MacPro. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ACMenuView;

@protocol ACMenuViewDataSource <NSObject>

- (NSInteger)numberOfItemsInMenuList:(ACMenuView *)menuList;
- (NSString *)menuList:(ACMenuView *)menuList titleForItemWithIndex:(NSInteger)index;

@end

@protocol ACMenuViewDelegate <NSObject>

- (void)menuList:(ACMenuView *)menuList didSelectButtonWithIndex:(NSInteger)index;

@end
@interface ACMenuView : UIView

@property (nonatomic) NSInteger selectedButtonIndex;

@property (nonatomic, weak) id<ACMenuViewDataSource> dataSource;
@property (nonatomic, weak) id<ACMenuViewDelegate> delegate;

- (void)initMenu;
- (void)reloadData;

@end


